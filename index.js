const express = require('express');
const mysql = require('mysql');
const app = express();
const port = 3000;

var db = mysql.createConnection({
      host: "localhost",
      user: "root",
      password: "rootpass",
      database: "testdb"
});

app.get('/', (req, res) => {

  db.query("SELECT * FROM question", (err, result, fields)=>{
    if (err) {
       console.log(err)
    } 
    console.log(result)
    let resText = "<div>Title:<div><p>";
    Object.keys(result).forEach(function(key) {
       var row = result[key];
       console.log(row.title)
       resText = resText + "<div>" + row.title + "</div>";
    });
    resText = resText + "</p>";
    res.send(resText);
  
  }); 

})

app.get('/about', (req, res) => {
  res.send('about')
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})