# express_mysql_ex1

This is the starting point of using express to query mysql example. 
There is no good architectural decision. Simply it is just try out example.

## Getting started
 - Create your database called "testdb" in MySQL server
 - Create your question table by using db.sql
 - Update your question table data by using db_update.sql
 - If your server does not have node packages, please run the following command
```
npm install
```
    - Then, your inside your folder, you can see "node_modules" folder 

## Run your script in node
- Run server script
```
npm start
```


## Using Git - add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/swen320_330/common/express_mysql_ex1.git
git branch -M main
git push -uf origin main
```

