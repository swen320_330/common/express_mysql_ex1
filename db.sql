CREATE TABLE testdb.question (
	id INT auto_increment NOT NULL,
	title varchar(100) NULL,
	content TEXT NULL,
	is_answered varchar(100) NULL,
	add_datetime TIMESTAMP NULL,
	CONSTRAINT question_PK PRIMARY KEY (id)
)
ENGINE=MyISAM
DEFAULT CHARSET=utf8
COLLATE=utf8_general_ci;
